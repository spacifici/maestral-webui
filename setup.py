import setuptools

with open('requirements.txt', 'r') as f:
    install_requires = f.read().splitlines()

setuptools.setup(name='maestral_webui',
                 packages=['maestral_webui'],
                 install_requires=install_requires)
