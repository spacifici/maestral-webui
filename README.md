# maestral-webui

A basic [maestral][1] webui.

I recently bought a Raspberry PI based NAS on which I installed
[openmediavault][2]. Unfortunately, I didn't manage to find any pre-existing
[Dropbox][3] plugin/integration with the system, plus I do not like too mush to
use [maestral][1] via ssh. So, I decided to write a very simple webui to be run
inside a [Docker][4] container on the NAS itself.

The webui is has only the few features that I personally need to have, it has
been written in Python ([Flask][5]). You can run it with the following command:

```sh
docker run -d --rm \
    -v /path/to/config/folder/or/volume:/data/home \
    -v /path/to/data/folder/or/volume:/data/dropbox \
    -e DBX_UID=`id -u` \
    -e DBX_GID=`id -g` \
    -p 8080:8080
    spacifici/maestral-webui:latest
```

If not given, the default `DBX_UID` and `DBX_GID` are both equal to 1000. The
image support i386, amd64, arm and arm64 architectures, so it runs out of the
box on Raspberry PI.

If you want to run it inside [Yacht][6] or [Portainer][7], you can find below
a docker compose yaml file example.

```yaml
version: '3.3'
services:
    maestral-webui:
        volumes:
            - '/path/to/config/folder/or/volume:/data/home'
            - '/path/to/data/folder/or/volume:/data/dropbox'
        environment:
            - DBX_UID=1000
            - DBX_GID=1000
        logging:
            driver: json-file
            options:
                max-size: 10m
        ports:
            - '8080:8080'
        image: spacifici/maestral-webui:latest
```



[1]: https://maestral.app/
[2]: https://www.openmediavault.org/
[3]: https://www.dropbox.com/
[4]: https://www.docker.com/
[5]: https://flask.palletsprojects.com/en/2.0.x/
[6]: https://yacht.sh/
[7]: https://www.portainer.io/
