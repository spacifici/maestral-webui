#!/bin/sh

rootdir=$(dir=`dirname $0`; cd "$dir/../"; pwd)
image_name="spacifici/maestral-webui:latest"

# Check if we can use arm64 images
echo Checking multiarch support
docker run --rm -t --platform linux/arm64 hello-world > /dev/null 2>&1

if [ $? -ne 0 ]; then
	echo "Loading binfmt and qemu support"
	docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
fi

echo Building images
{
	cd ${rootdir}
	docker buildx build \
		--platform linux/amd64,linux/386,linux/arm,linux/arm64 \
		-t spacifici/maestral-webui:latest .
}
