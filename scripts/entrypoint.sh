#!/bin/sh

dbx_uid=${DBX_UID:-1000}
dbx_gid=${DBX_GID:-1000}

addgroup -g ${dbx_gid} dropbox
adduser -h /data/home -s /bin/false -G dropbox -D -u ${dbx_uid} dropbox
chown -R ${dbx_uid}:${dbx_gid} /data/home
chown -R ${dbx_uid}:${dbx_gid} /data/dropbox

export MAESTRAL_WEBUI_CONFIG_PATH=/data/home/webui.ini
export MAESTRAL_DROPBOX_PATH=/data/dropbox

source /maestral-webui-env/bin/activate
cd /maestral_webui
export FLASK_APP=app
su -s /bin/sh dropbox -c "flask run -h 0.0.0.0 -p 8080"
