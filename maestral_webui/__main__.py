from . import PORT, app

if __name__ == "__main__":
    app.app.run(port=PORT, debug=True)
