"""
This is the maestral_webui module
"""

from pathlib import Path
from os import getenv

ROOT_DIR = str(Path(__file__).absolute().parent)

PORT = int(getenv("MAESTRAL_WEBUI_PORT", "8080"))

CONFIG_NAME = getenv("MAESTRAL_WEBUI_CONFIG_NAME", "default")

WEBUI_CONFIG_PATH = Path.home() / ".config" / "maestral-webui" / "config.ini"
_env_config_path = getenv("MAESTRAL_WEBUI_CONFIG_PATH")
if _env_config_path:
    WEBUI_CONFIG_PATH = Path(_env_config_path)

DROPBOX_DIR = Path.home().joinpath("Dropbox ({})".format(CONFIG_NAME))
_env_dropbox_dir = getenv("MAESTRAL_DROPBOX_PATH")
if _env_dropbox_dir:
    DROPBOX_DIR = Path(_env_dropbox_dir)
