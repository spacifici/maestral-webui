from . import CONFIG_NAME, DROPBOX_DIR, WEBUI_CONFIG_PATH

from configparser import ConfigParser
from flask import Flask, request, Response, render_template, redirect, url_for
from maestral.errors import NotLinkedError, NoDropboxDirError
from maestral.main import Maestral
from collections.abc import Iterator
from typing import Any


app = Flask(__name__)
maestral = Maestral(config_name=CONFIG_NAME)
linked = False
syncing = False

# An easy way to check if we are connected is to get history
try:
    maestral.get_history(limit=1)
    linked = True
except NotLinkedError:
    print("Not linked")


def start_sync():
    global syncing
    while True:
        try:
            maestral.start_sync()
            syncing = True
            print("Sync started")
            store_config()
            break
        except NoDropboxDirError:
            maestral.create_dropbox_directory(DROPBOX_DIR)
            print("{} directory created".format(DROPBOX_DIR))


def stop_sync():
    global syncing
    maestral.stop_sync()
    syncing = False
    store_config()


def store_config():
    global syncing
    abs_path = WEBUI_CONFIG_PATH.absolute()
    parent = abs_path.parent
    if parent.exists() and not parent.is_dir():
        raise NotADirectoryError()
    if not parent.exists():
        parent.mkdir(parents=True)
    parser = ConfigParser()
    parser.add_section("sync")
    parser.set("sync", "enabled", str(syncing))
    with open(abs_path, "w") as f:
        parser.write(f)


def read_config():
    global syncing
    parser = ConfigParser()
    parser.read(WEBUI_CONFIG_PATH.absolute())
    syncing = parser.getboolean("sync", "enabled", fallback=False)


read_config()
if linked and syncing:
    start_sync()


def list_root():
    for ls in maestral.list_folder_iterator("/", recursive=False):
        for item in ls:
            yield item


@app.route("/", methods=["GET", "POST"])
def index() -> Response:
    global linked, syncing
    unlink = int(request.args.get("unlink", 0)) > 0
    sync = request.args.get("sync", "")
    if request.method == "POST":
        included = set(request.form.getlist("file"))
        all = set(request.form.getlist("filepath"))
        excluded = all.difference(included)
        if excluded:
            maestral.excluded_items = list(excluded)
        start_sync()
    if linked and unlink:
        # Unlink here
        stop_sync()
        maestral.unlink()
        linked = False
    if linked and syncing and sync == "stop":
        # Stop syncing
        stop_sync()
    content = []
    if linked and not syncing:
        content = list_root()
    return render_template("index.html",
                           is_linked=linked,
                           is_syncing=syncing,
                           root_content=content)


@app.route("/link", methods=["GET", "POST"])
def link() -> Response:
    url = maestral.get_auth_url()
    if request.method == "GET":
        return render_template("link_form.html", url=url)
    auth_code = request.form["auth_code"]
    if not auth_code:
        return render_template("link_form.html",
                               url=url,
                               error="Empty auth code")
    global linked
    res = maestral.link(auth_code)
    if res == 0:
        linked = True
        maestral.create_dropbox_directory(DROPBOX_DIR)
        return redirect(url_for("index"))
    if res == 1:
        return render_template("link_form.html",
                               url=url,
                               error="Invalid auth code")
    if res == 2:
        return render_template("link_form.html",
                               url=url,
                               error="Can't connect to Dropbox")
    return render_template("link_form.html",
                           url=url,
                           error="Unknown error code {}".format(res))
