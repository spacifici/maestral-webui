FROM python:alpine AS build

COPY requirements.txt /
RUN apk add build-base libffi-dev openssl-dev cargo rust \
&&  python -m venv /maestral-webui-env \
&&  source /maestral-webui-env/bin/activate \
&&  pip install -r requirements.txt

FROM python:alpine AS main

COPY --from=build /maestral-webui-env /maestral-webui-env
COPY maestral_webui /maestral_webui
COPY scripts/entrypoint.sh /

VOLUME /data/home
VOLUME /data/dropbox

ENTRYPOINT /entrypoint.sh
